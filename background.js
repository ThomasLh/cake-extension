// Initialize Firebase
var config = {
    apiKey: "AIzaSyCvLXPZdLPAYL8ke7Cl-GrLBiA6xw0pHZc",
    authDomain: "test-chat-cf0e1.firebaseapp.com",
    databaseURL: "https://test-chat-cf0e1.firebaseio.com",
    storageBucket: "test-chat-cf0e1.appspot.com"
};
firebase.initializeApp(config);

// Initialize Firebase.
function initApp() {
    // Listen for auth state changes.
    firebase.auth().onAuthStateChanged(function(user) {
        console.log('User state change detected from the Background script :', user);
    });
}

window.onload = function() {
    initApp();
};

// Commonly-used Firebase references.
var chatRef = firebase.database().ref();
var _messageRef = chatRef.child('messages');

// Register the cake tab status.
var senderTabId = 0;
var isCakeOpened = false;
var isCakeInit = false;
var isCakeActive = false;


// A Chrome tab emited a message.
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    //console.log("onMessage event -- sender = ", sender, " - sender.tab.id", sender.tab.id);
    senderTabId = sender.tab.id;
    if(sender.id == "jmolfedfnladcndbookfnldhcajgjpnp") {
        //console.log("request.init =", request.init, " - request.focus =", request.focus);
        if (request.init || request.focus && !isCakeOpened) {
            isCakeOpened = true;
        }
        isCakeActive = request.focus;
        //console.log("isCakeOpened =", isCakeOpened, " - isCakeActive =", isCakeActive);
    }
});

// @TODO check if cake tab has been closed
// A chrome tab has been closed.
chrome.tabs.onRemoved.addListener(function(tabId, removeInfo) {
    console.log("onRemoved event - removeInfo = ", removeInfo);
});

// A chrome tab has been activated.
chrome.tabs.onActivated.addListener( function(info) {
    var tabId    = info.tabId,
        windowId = info.windowId;
    //console.log("onActivated event - info = ", info);
});

var newMessages = false;

// Send a chrome notification when user is logged cake opened
// but tab inactive.
_messageRef.on('child_added', function(snapshot) {
    if (!newMessages) return;
    if(isCakeOpened && !isCakeActive) {
        //console.log(snapshot.val());
        sendNotification(snapshot.val());
    }
});

_messageRef.once('value', function(messages) {
  newMessages = true;
});

// Display the notification.
function sendNotification (message) {
    var notification = new Notification( message.name, {
        icon: message.photoURL,
        body: message.message
    });

    chrome.runtime.sendMessage({notification: true}, function (response) {});
}

/*// A chrome tab has been updated (url change, ... ).
 chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
 console.log("onUpdated event");
 console.log(changeInfo);
 });*/