chrome.runtime.getBackgroundPage(function(window){
    var background = window;
});

chrome.runtime.sendMessage({init: true, focus: true}, function (response) {});

var Cake = function (options) {

    // On window focus, sendMessage to background.js
    $(window).focus(function() {
        chrome.runtime.sendMessage({init: false, focus: true}, function (response) {});
    });
    // On window blur (focus lost), sendMessage to background.js
    $(window).blur(function() {
        chrome.runtime.sendMessage({init: false, focus: false}, function (response) {});
    });

    // On message received from background.js
    chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
        if(sender.id == "jmolfedfnladcndbookfnldhcajgjpnp") {
            // @TODO Set dynamic favicon notification
        }
    });

    // Base Cake configuration.
    var self = this;

    this._user               = null;
    this._userId             = null;
    this._userName           = null;
    this._photoURL           = null;

    this.maxLengthMessage    = 250;
    this.numMaxMessages      = 20;
    this.chatHeight          = 0;

    this.messagesContainer   = $("#messages-container");
    this.loadingContainer    = $(".loading-container");
    this.textarea            = $(".message-textarea");

    // Set the Firebase config and initialize Firebase.
    var config = {
        apiKey: "AIzaSyCvLXPZdLPAYL8ke7Cl-GrLBiA6xw0pHZc",
        authDomain: "test-chat-cf0e1.firebaseapp.com",
        databaseURL: "https://test-chat-cf0e1.firebaseio.com",
        storageBucket: "test-chat-cf0e1.appspot.com"
    };

    firebase.initializeApp(config);

    // Commonly-used Firebase references.
    this.chatRef     = firebase.database().ref();
    this._messageRef = this.chatRef.child('messages');
    this._userRef    = null;
    this.roomMessages = {};
    this.roomMessagesKeys = [];

    this.target = document.getElementById("chat-container");

    // AUTHENTICATION
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) { // User is signed in.
            self._user     = true;
            self._userId   = user.uid;
            self._userName = user.displayName;
            self._photoURL = user.providerData[0].photoURL ? user.providerData[0].photoURL : user.photoURL;

            // console.log("user logged : " + user.displayName);
            // @TODO initUserView(user);
        } else { // No user is signed in.
            // console.log("user not logged");
            // @TODO initUserView(false);
        }
    });

    // MESSAGE LISTENERS
/*    this.query = self._messageRef.orderByChild("timestamp").limitToLast(10); //this.numMaxMessages
    this.query.on("child_added", function(snapshot) {

        //console.log(snapshot.val());
        
        // Add the message object in the roomMessages object with timestamp-key as key.
        var message_key       = snapshot.key; // .key , .key() ?
        var message_value     = snapshot.val();
        var message_timestamp = message_value.timestamp;

        var local_message_key = message_timestamp + message_key;

        self.roomMessages[local_message_key] = message_value;
        self.displayNewMessage(message_key, local_message_key);
    });*/

    // OLD - WORKING FUNCTION
    this._messageRef.once('value', function(snapshot) {

      // Child added
        self._messageRef.limitToLast(self.numMaxMessages).on('child_added', function(snapshot) {
            self.displayNewMessage(snapshot);
        }, /* onCancel */ function() {
         // Turns out we don't have permission to access these messages.
            //self.leaveRoom(roomId);
        }, /* context */ self);

      // Child changed
        // -- Firebase fires two local events for the write operation :
        // -- It immediately fires a child_added event with the local timestamp (corrected for your expected offset to the server).
        // -- It later fires a child_changed event with the actual timestamp as the server specified it.
        // see http://stackoverflow.com/questions/34196263/firebase-servervalue-timestamp-not-synched-between-listeners-and-the-client-that
        self._messageRef.limitToLast(self.numMaxMessages).on('child_changed', function(snapshot) {
            console.log('key', snapshot.key);
            console.log('val', snapshot.val());
            self.updateMessage(snapshot);
        }, /* onCancel */ function() {
            // Turns out we don't have permission to access these messages.
            //self.leaveRoom(roomId);
        }, /* context */ self);

      // Child removed
        self._messageRef.limitToLast(self.numMaxMessages).on('child_removed', function(snapshot) {
            self.removeMessage(snapshot);
        }, /* onCancel */ function(){}, /* context */ self);

    }, /* onFailure */ function(){}, self);

    Cake.prototype.insert = function(element, array) {
      array.splice(self.locationOf(element, array) + 1, 0, element);
      return array;
    }

    Cake.prototype.locationOf = function(element, array, start, end) {
        start = start || 0;
        end = end || array.length;
        var pivot = parseInt(start + (end - start) / 2, 10);
        if (array[pivot] === element) return pivot;
            if (end - start <= 1)
                return array[pivot] > element ? pivot - 1 : pivot;
            if (array[pivot] < element) {
                return self.locationOf(element, array, pivot, end);
        } else {
            return self.locationOf(element, array, start, pivot);
        }
    }

    // DISPLAY MESSAGE
    // Make previous element accessible.
    this.prev_message = false;
    this.separatorIsLast = false;
    // Display new message.
    Cake.prototype.displayNewMessage = function(snapshot) {

        //var index = Object.keys(self.roomMessages).indexOf(local_message_key);

        // Add the message object in the roomMessages object with timestamp-key as key.
        var message_key       = snapshot.key; // .key , .key() ?
        var message_value     = snapshot.val();
        var message_timestamp = message_value.timestamp;

        var local_message_key = message_timestamp + message_key;

        // Add the message object to the roomMessages objetc
        self.roomMessages[message_key] = message_value;
        // Add the message to the ordered array
        self.insert(local_message_key , self.roomMessagesKeys);

        // console.log("child_added");
        // console.log(self.roomMessagesKeys);
        // console.log(message_value);

        // Get the index (location) of the message to display
        var index = self.locationOf(local_message_key, self.roomMessagesKeys);

        //console.log("child added - index : " + index);


        // console.log(message);
        // var message = snapshot.val();
        var message = self.roomMessages[message_key];

        var message_date = new Date(message.timestamp);
        var prev_message_date;

        // @TODO put back time separators

        // Before displaying the message,
        // we check if a daily separator is needed.

        // Separators are added before the first message, 
        // and beetwin messages of different days.
        //---if there is a previous message in the view
        // if (self.prev_message) { 
        //     prev_message_date = new Date(self.prev_message.timestamp);

        //     if (prev_message_date > message_date) {

        //     } else if (message_date.toDateString() !== prev_message_date.toDateString() && prev_message_date > message_date) {
        //         self.displayDaySeparator(message_date);
        //     }
        // //---so it's the first message of the view
        // } else {
        //     self.displayDaySeparator(message_date);
        // }

        // Then we can construct the message html output 
        var el = "";
        el += "<li class='message";

        if (message.userId == self._userId) { // message to self
            el += " message--self";
        }

        // If previous message is also from actual user
        // group it (visually) with the current message
        if (self.prev_message && self.prev_message.userId == message.userId && !self.separatorIsLast && self.prev_message.timestamp < message.timestamp) {
            $(self.messagesContainer).find(".message").last().addClass("message--joined-bottom");
            el += " message--joined-top'";
        }

        el += "' data-userId = '";
        el += message.userId;

        el += "' data-msgKey = '";
        el += message_key;

        el += "'>";

        el += "<div class='message__info'>";

        el += "<img class='message__author-img' src='";
      if(message.photoURL) {
        el += message.photoURL;
      } else {
        // Default avatar.
        el += "../img/avatar_placeholder.png";
      }

        el += "'>";

        el += "<div class='message__author'>";
        el += message.name;
        el += "</div>";

        el += "<div class='message__timestamp'>";

        el += "<span class='message__timestamp--date'>";
        el += self.convertToDayNum(message_date);
        el += " ";
        el += self.convertToMonth(message_date);
        el += " ";
        el += "</span>";
        el += "<span class='message__timestamp--time'>";
        el += self.convertToTime(message_date);
        el += "</span>";

        el += "</div>";

        el += "</div>";

        el += "<div class='message__content'>";
        // Check if message contains a valid url, transform it in a proper a tag.
        el += self.urlify(message.message);
        el += "</div>";

        // @TODO set messages tags
        // el += "<div class='message__tags'>";
        // el += message.tags;
        // el += "</div>";

        el += "</div>";

        if (index != 0) { // if there is a previous message in the view
            self.messagesContainer.find(".message").eq(index-1).after(el);
        } else {          // else it's the first message of the view
            self.messagesContainer.prepend(el);
        }

        self.separatorIsLast = false;
        self.prev_message = message;

        // Scroll to the bottom.
        self.messagesContainer.stop().animate({scrollTop: self.messagesContainer[0].scrollHeight + self.messagesContainer.css('padding-top')}, 1000, 'swing', function() {
        });
    };

    // UPDATE MESSAGE
    Cake.prototype.updateMessage = function(snapshot) {
        var message_key       = snapshot.key; // .key , .key() ?
        var message_value     = snapshot.val();
        var message_timestamp = message_value.timestamp;

        var local_message_key = message_timestamp + message_key;

        console.log("message updated");

        if (self.roomMessages[message_key]) { // if message exist
            var prev_timestamp = self.roomMessages[message_key].timestamp;

            // If the message timestamp has been changed.
            if (message_timestamp != prev_timestamp) {
                // Update timestamp.
                // Loop through the ordered array to find and update the message key.
                for (var i=0; i < self.roomMessagesKeys.length; i++) {
                    // If a key of the ordered array contains the message key.
                    if (self.roomMessagesKeys[i].includes(message_key)) {
                        // Then update both roomMessages and roomMessagesKey with the new value.
                        self.roomMessagesKeys[i] = local_message_key;
                        self.roomMessages[message_key].timestamp = message_timestamp;
                    }
                } 
            }

            // @TODO update message content.
        }
    }

    // REMOVE MESSAGE
    Cake.prototype.removeMessage = function(snapshot) {
        var message_key       = snapshot.key; // .key , .key() ?
        var message_value     = snapshot.val();
        var message_timestamp = message_value.timestamp;

        var local_message_key = message_timestamp + message_key;
        var index = self.roomMessagesKeys.indexOf(local_message_key);

        // Check adjacent sibling and apply grouping classes if needed.
        if(self.roomMessagesKeys[index-1]) { // if there is a previous message in the view
            self.messagesContainer.find(".message").eq(index-1).removeClass("message--joined-bottom");
        }
        if(self.roomMessagesKeys[index+1]) { // if there is a nex message in the view
            self.messagesContainer.find(".message").eq(index+1).removeClass("message--joined-top");
        }

        // Remove message_key from the ordered array.
        if (index > -1) {
            self.roomMessagesKeys.splice(index, 1);
        }

        // Remove message object from the roomMessages object.
        delete self.roomMessages[message_key];

        // Remove message from the view.
        var message = self.messagesContainer.find("[data-msgKey='" + message_key + "']");
        message.remove();

    };

    Cake.prototype.displayDaySeparator = function(date) {
        self.separatorIsLast = true;

        var date_str = "";
        date_str += "<div class='message__day-separator'>";
        date_str += self.convertToWeekDay(date);
        date_str += " " + self.convertToDayNum(date);
        date_str += " " + self.convertToMonth(date);
        date_str += "</div>";

        self.messagesContainer.append(date_str);
    }

    // UNIX TIMESTAMP CONVERTING METHODS
    // Convert unix timestamp into date. e.g. j/m/a
    Cake.prototype.convertToDate = function(time) {
        var date = new Date(time);

        var todate        = date.getDate();
        var tomonth       = date.getMonth()+1;
        var toyear        = date.getFullYear();

        var formattedDate = todate+'/'+tomonth+'/'+toyear;

        return formattedDate;
    };

    // Convert unix timestamp into hours : minutes. e.g : 22:34
    Cake.prototype.convertToTime = function(time) {
        var date = new Date(time);

        var hours         = date.getHours();
        var minutes       = "0" + date.getMinutes();
        var seconds       = "0" + date.getSeconds();

        var formattedTime = hours + ':' + minutes.substr(-2);

        return formattedTime;
    };

    // Convert unix timestamp into day of the week. e.g : Lundi
    Cake.prototype.convertToWeekDay = function(time) {
        var date = new Date(time);

        var weekday = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
        var n = weekday[date.getDay()];

        return n;
    };

    // Convert unix timestamp into number of the day. e.g : 31
    Cake.prototype.convertToDayNum = function(time) {
        var date = new Date(time);
        var n = date.getDate();

        return n;
    };

    // Convert unix timestamp into month of the year. e. g : Septembre
    Cake.prototype.convertToMonth = function(time) {
        var date = new Date(time);

        var month = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
        var n = month[date.getMonth()];

        return n;
    };

    // MESSAGE CONVERTING METHODS
    // Search string for valid url to be wrapped in a tag.
    Cake.prototype.urlify = function(text) {
        var urlRegex = /(https?:\/\/[^\s]+)/g;
        return text.replace(urlRegex, function(url) {
            return '<a href="' + url + '">' + url + '</a>';
        })
    };

    // Send message.
    Cake.prototype.sendMessage = function(messageContent, messageType, cb) {
        var self = this,
            message = {
                userId: self._userId,
                name: self._userName,
                photoURL: self._photoURL,
                timestamp: firebase.database.ServerValue.TIMESTAMP,
                message: messageContent,
                tags: [],
                type: messageType || 'default'
            },
            newMessageRef;

        if (!self._user) {
            alert('Not authenticated or user not set!');
            return;
        }

        newMessageRef = self._messageRef.push();
        newMessageRef.setWithPriority(message, firebase.database.ServerValue.TIMESTAMP, cb);
    };

    // Attach on-enter event to textarea.
    var $textarea = this.textarea;

    $textarea.bind('keydown', function(e) {
      var message = self.trimWithEllipsis($textarea.val(), self.maxLengthMessage);
      if ((e.which === 13) && (message !== '')) {
        $textarea.val('');
        self.sendMessage(message);
        return false;
      }
    });

    var remove_button = "<i class='icon-cross message__delete-button'></i>";

    $('body').on('mouseenter', 'li.message', function(event) {
      //console.log('hover');
      if (self._userId === $(this).attr("data-userId")) {
        $(this).append(remove_button);
      }
    });

    $('body').on('mouseleave', 'li.message', function(event) {
      if (self._userId === $(this).attr("data-userId")) {
        $(this).find(".message__delete-button").remove();
      }
    });

    $('body').on('click', '.message__delete-button', function(event) {
        var key = $(this).parent().attr("data-msgKey");
        if (self._userId === $(this).parent().attr("data-userId")) {
            self.deleteSelfMessage(key);
        }
    });

    Cake.prototype.deleteSelfMessage = function(oldChildSnapshotkey) {
        //console.log("removeMessage ", oldChildSnapshotkey);
        var messageRef = firebase.database().ref("messages/" + oldChildSnapshotkey);

        messageRef.remove().then(function() {
            //console.log("Remove succeeded.");
        })
        .catch(function(error) {
            //console.log("Remove failed: " + error.message);
        }); 
    };


    /**
     * Remove leading and trailing whitespace from a string and shrink it, with
     * added ellipsis, if it exceeds a specified length.
     *
     * @param    {string}    str
     * @param    {number}    length
     * @return   {string}
     */
    Cake.prototype.trimWithEllipsis = function(str, length) {
        str = str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
        return (length && str.length <= length) ? str : str.substring(0, length) + '...';
    };

    var view_height = this.chatHeight;
    var messages_container = this.messagesContainer;
    var window_height = $(window).height();
    var textarea_height = this.textarea.outerHeight();
    var menu_height = $('.top-menu').height();

    if(!this.chatHeight) {
        // If chatHeight option false :
        // Set messages container height equal to window height.
        messages_container.css({ 'height' : window_height - textarea_height - menu_height });

        // Add window resize event listener.
        window.addEventListener("resize", function(){
            window_height = $(window).height();
            messages_container.css({ 'height' : window_height - textarea_height - menu_height });
        }, true);

    } else {

    }
};

// Launch app
$(document).ready(function() {
    var cake = new Cake();
});